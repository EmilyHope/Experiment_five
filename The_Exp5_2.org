#+title: Breath
#+author: Emily Hope
#+date: <2020-01-24 Fri>


* Chapter 1 - Breathe

#+BEGIN_VERSE

Stop. Stop Right now and do something else.  There will be no more warnings. This is not a drill.  There is no green pill. This  is your choice.

Right now, in this moment, you are alive. Let that be true! at least that, and if not now, then answer me, 'i have never had it so good'.

Make it so
Tea Earl Grey hot

and are you sitting 
wherever you are
whomever you are
wherever your goals

let me ask

"how are you'?
Doesn't it feel good to be alive?

Isn't there anything better you could be doing? No? OK then. you have been warned. [let the record show that the reader (dear reader) had been warned.]

<mic tap>

strap in, take of your shoes[ha!], clear your mind[fuck that shit], make tea, whatever.
take this moment and 
breath.

I ask again (and still cannot know)

How are you? right now, in this moment. i am asking how you feel.

this is not some scripted daily interaction that you can automate through whilst thinking about something else. well i guess is it, but this is my request. humour me.

In this very moment, ‘What are your needs`?

[what does that even mean?]

breathe.

There are reasons why children's stories begin with, Are you sitting comfortably?'[check] Listen, to pay attention your basic needs must be met. And I would really love your attention[roger] yes you are warm and dry and fed[check]. you might even have a cup of tea [wait one - ready]or some other beverage. so lets go beyond that.[cleared for take off] <see you on the flip side>

so yes, thats right, lets take a moment to relax. the story will be here[yes there is a fucking story here], there when we return. lets take moment and consider how we are feeling.  

And how I might serve you?

my stated goals are: I believe that `i might contribute to your wellbeing.  I would, most of all love to offer you hope. or humour. at least that. the politics of whimsy. and 

breathe.

so. lets try this.

stop reading and close your eyes for a moment. but before you do, briefly note any competing things lurking at the back of your mind, just for a moment. [I know you know how to do this now] Observe. 

If like me, you were surrounded by multiple competing anxieties, then come with me, on a journey. it can be our little secret. lets distract ourselves from all that nasty stuff out there in the real world.

and breathe the air. deep.
let the competing things be quieter, they, like this story, will still be there when we are done.



isn't that nice?

Well I hope it is, especially the air. Its kinda useful. And if not right now, at least you have a memory of clean fresh sweet natural air. A good memory I trust? And perhaps when you soon get some of that kind of air again perhaps think of me, of us. Together now. Our little secret.

and relax

#+END_VERSE

#+CAPTION: Montana Wildhack looks out to sea, meditating.
[[file:images/montana_wildhack.jpg]]

Yes there we are.\\

\smiley

I hope that you are curious. where might we go? well, Do you like weird? Are you scared of the dark?





-----

[[file:The_Exp5_3.org][Next]]
