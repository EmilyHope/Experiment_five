#+title: Today
#+author: Emily Hope
#+date: <2020-01-24 Fri>



* Chapter 4 Today

** Communication with Friend D
[REDACTED]
** Communication with Friend F
[REDACTED]
** About Teviot
carpae Dieum

** Experiment 4

All the preconditions are met.

The reader has been warned.

You have no permission to proceed.

A nexus has aligned in the time space thing.

Beer [check]
seat [check]
battery levels [Check]
Body temperature [falling - Estimated window 30 mins]

[Engage]


a context. I was sat there in my favourite place.  I had done some great work that is relevant here [REDACTED] And then this song came on the radio. 

And since then, things have been /different/. I have moved in and out of this strange place.  I have had the song on loop, the album on loop. I have written more than I have written in years.  I have become unstuck in time, unsane. And that is OK.


** Some Time ago. Of an evening. Two folk chat.
[REDACTED]

When I was introduced to FR last year by Friend P I texted Friend C in a moment of imbibement. I was hit so hard by the lyrics. I was less alone. 

Friend P and I were chatting one night. It was intense.  Suddenly they chose to change the musical experience. <How unusual!>, you know what its like - one of those, 'i have to listen to this song *now*, do you mind?' kinda situations.

And wow, what a song.

I was so moved that I had actually just recorded the song on my phone and sent it to Friend C. They loved it. 

This is the power of connection.


** Teviot - Now - Today

And this is the song that came on the radio at Teviot at that moment.

here check this out...

** Firghtened Rabbit - Floating in the Forth

#+BEGIN_VERSE
[Verse 1]
So you just stepped out
Of the front of my house
And I'll never see you again
I closed my eyes for a second
And when they opened
You weren't there
And the door shut shut
I was vacuum packed
Shrink-wrapped out of air
And the spine collapsed
And the eyes rolled back
To stare at my starving brain

[Chorus]
And fully clothed, I float away
(I'll float away)
Down the Forth, into the sea
I think I'll save suicide for another day

[Verse 2]
And I picture this corpse
On the M8 hearse
And I have found a way to sleep
On a rolled up coat
Against the window
With the strobe of the sun
And the life I've led
Am I ready to leap
Is there peace beneath
The roar of the Forth Road Bridge?
On the northern side
There's a Fife of mine
And a boat in the port for me

[Chorus]
Fully clothed, I'll float away
(I'll float away)
Down the Forth, into the sea
I'll steer myself
Through chopping waves
As manic gulls
Scream "it's okay"
Take your life
Give it a shake
Gather up
All your loose change
I think I'll save suicide for another year[Chorus]
And fully clothed, I float away
(I'll float away)
Down the Forth, into the sea
I think I'll save suicide for another day


#+END_VERSE

Here is a copy of the song online:
[[https://www.youtube.com/watch?v=zGsYK3xSkio][Frightened Rabbit Floating in the Forth (YouTube)]]


** wot i writ today

TL;DR i sent out a text message. It should describe what just happened.

The moment passed for everyone else in the bar with no noticeable effect. How could such a song go unmissed by the students?

To meet my need for connection I marked the moment with a text:

#+BEGIN_VERSE
i am in the student union at edinburgh uni. Teviot. the library bar. it has a mezznine, it is very old, and used to be a library. there are old book  cases, some hsve foe old books in, all have coloured lights.  over the speaker the band Frighten Rabbit sings, canned, tinny amid the wooden walls.  The singer sings about suicide.  as per his lyrics he was found in the forth a few years ago.
2m
and i want to smoke.
2m
an i want to smoke
1m
the juxtapositions.
1m
sat in this moment. entering a nexus. between parallel universes. to slide, with acuity between potential realities/ futures/hopes.
1m
and write.
1m
to my friends.
1m
with love
1m
XX

#+END_VERSE


PICTURE

And I sent this to Friend C and Friend B.


** Friend C

i have known her for 20+ years. 

She doesn’t know I am writing this. I am worried, her phone is off. Its never off. If she died no-one would tell me.

I met her when I was 21. it was a special time. we were thin, there was hope. I was learning to sail and sailing was healing me.

When we found out we were both depressed and suicidal we got to know each other. We wrote. 30 years later TL;DR She got addicted to smack. She robbed a jewellery store. And did time. She got off smack. Got a flat. And a dog. And a guitar. And a cough.

We write and we chat. I send her stuff.

Latterly she has started to get in the levellers and their festival. : )  like totally an its a delight to see the joy of the discovery of the festival experience and the joy of the levellers. They were an influence for me in the 90’s. That time of hope and hope for freedom.  Yes I was one of those who fucked it up for the \real\ travellers. I tried to join their crusty ranks. Or at least, find a better way of living.

But the reason that we connected was because we were both \jumpers\.

She still has a scar on her face from where she hit the train line. She pushed off the bridge, thinking happy peaceful thoughts. Then was annoyed to wake up in hospital. More than annoyed, angry as fuck.

We can joke about it. Its kinda funny.  When I phoned Friend C to check this (its all true), we did joke about it. not that its funny. Not really.

Its shit. Its shit that kids have shit childhoods such that they dont want to live. And this is the toxic treasure. The <device>, the potential, the hope. if we flip that corroloray, maybe our shitty childhoods will not be in vain.


** An important notice to parents

There is no blame here.

There is a segue to grief work. It will need a fuller explanation. It is coming.


** segue comment
So thisis the first comment on the youtube link to the vidoe. I find this as I write this up. It is relevant. Since it shares Freinds C's experience.  Peace. 

#+BEGIN_QUOTE
mathdebaterclub
1 year ago
You know how they say people sometimes regret jumping to their deaths immediately during the fall? I really hope that wasn't the case with Scott. I hope that his last few seconds of life were a relief to him, knowing that all of his problems would be gone; I hope that he really did find peace beneath the roar of the forth road bridge, if only for a moment. 
#+END_QUOTE

** Recently on BBC Radio 4.

#+BEGIN_QUOTE

"The first in a three-part series on music and mental health, Playing Well, Chris Hawkins examines the life and legacy of the Frightened Rabbit lead singer Scott Hutchison.

in particular, 'Charting the exhausting aftermath of suicide, Grant talks about defining Scott as a songwriter, in the hope that the existence of works which appear to presage his death don't create a misleading impression of Scott's life. '"

#+END_QUOTE

https://www.bbc.co.uk/programmes/m0009zby

When I found this BBC thing.  I was driving home. I was so lucky. A lovely happenstance to hear it live on the radio. 

** Later things that day.
[redacted]

** the deneument. The next message
Earlier I explained my reason for being in town, was to go to a meeting, an NVC meeting. 

The next message that I received was from Friend W, to alert me to potential cancellation of the meeting. In the three years that I have been a member, this man has never let the group down, because when they have not been available, they have pre-organised alternative arrangements.  That is to say that this was the first time ever that the group in my experience was canceled. 

This is their message:

"Em, owing to an accident or suicide on the track, the train I’m on is at a stop. If it doesn’t resume within 30 mins of now (1640) I shall be late, and I have both keys! I’m telling you this before I tell the others because You might be coming into the city specially, and I want to avert the jeopardy of you spending valuable time and money on a wasted journey. I’ll keep you posted by email with the others"

I sent him pictures of the place that I intend to use for grief ritual. 

I asked him how he was.

"I have something between dismay and alarm lest suicide by this method (and maybe by all methods) be on the rise... x"

So to be clear, NVC was disrupted that evening because of suicide. Or maybe it wasn't. Maybe something else happened?


-----
[[file:The_Exp5_8.org][Next]]
